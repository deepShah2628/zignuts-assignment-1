import React,{useState,useEffect} from 'react';
import './App.css';
import Login from './component/Login'
import Update from './component/Update'
import Logout from './component/Logout'
import { Route, BrowserRouter as Router,Redirect, Switch } from 'react-router-dom';
import './App.css';
const App =() =>{
 const [isLogin, setIsLogin] = useState(false)
    
  useEffect(() => {
    if(localStorage.getItem('IsLogin') === '1')
    {
        setIsLogin(true);
    }

  }, [])
 
  const setAuth = (userObject) =>
  {
      setIsLogin(true)
      localStorage.setItem('IsLogin',1);
      localStorage.setItem('user',JSON.stringify(userObject));
      
  }

 const setDestroyAuth = () =>
  {
      setIsLogin(false)
      localStorage.removeItem("IsLogin");
      localStorage.removeItem("user");
  }

    return (
      <div>
      <Logout setAuth={setDestroyAuth} setFlag={isLogin} />
        <Router >
        <Switch>
        {isLogin ?
             
             <Route exact path="/" component={Update}/>
                    
          :
          <Route exact path="/" component={()=> <Login setAuth={(userObject) => setAuth(userObject)} />}/>
              
        }
        </Switch>
        </Router>
      </div>
    )
  
      }


export default App;
