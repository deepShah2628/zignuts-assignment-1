import React,{useState,useEffect} from 'react';
import JSONINVITATION from '../JSON/invitations.json'
import JSONUPDATE from '../JSON/invitations_update.json';
import DataShow from './DataShow';

 const Update = () =>{

    const [Update, setUpdate] = useState(JSONINVITATION.invites)
            
        

   const lookIntoData =() =>
    {
     
        let getUserDetail = localStorage.getItem('user');
        let selectedData = Update.filter(jsonData => jsonData.sender_id === JSON.parse(getUserDetail).first_name);

        return selectedData.map(function(object, i){
            return <DataShow obj={object} key={i} />;
        })
    }

    useEffect(() => {
        const getInvitationUpdate = JSONUPDATE.invites
          setTimeout(() => {
              setUpdate(Update.concat(getInvitationUpdate))
             
            }, 5000);
    }, [])
    
    
        return(
            <div id="userLogin">
               
                <div>
                    {lookIntoData()}
                </div>
            </div>
        )
    }



export default Update;