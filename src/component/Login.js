import React, { useState } from "react";
import userjson from "../JSON/users.json";

const Login = (props) => {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");

  const getUsername = (e) => {
    setUsername(e.target.value);
  };

  const getPassword = (event) => {
    setPassword(event.target.value);
  };

  const onSubmitData = () => {
    let usernameData = username;
    let passwordData = password;

    const getJsonRead = userjson;
    console.log(getJsonRead);
    let selectedData = getJsonRead.users.filter(
      (jsonData) => jsonData.email === usernameData
    )[0];

    if (selectedData) {
      let jsonUsername = selectedData.email;
      let jsonPassword = selectedData.password;

      if (jsonUsername === usernameData && jsonPassword === passwordData) {
        props.setAuth(selectedData);
      }
    } else {
      alert("Wrong Username & Password");
    }
  };

  return (
    <div className="main">
      <p className="sign" align="center">
        Sign in
      </p>

      <input
        className="un "
        value={username}
        onChange={getUsername}
        type="text"
        align="center"
        placeholder="Username"
      />
      <input
        className="pass"
        value={password}
        onChange={getPassword}
        type="password"
        align="center"
        placeholder="Password"
      />
      <a className="submit" onClick={onSubmitData} align="center">
        Sign in
      </a>
      <p className="forgot" align="center">
        <a href="#">Forgot Password?</a>
      </p>
    </div>
  );
};

export default Login;
